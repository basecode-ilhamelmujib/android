package id.basecode.android.data.models

data class CharacterLocationEntity(
    val name: String,
    val url: String
)
