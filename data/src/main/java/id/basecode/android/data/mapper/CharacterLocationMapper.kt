package id.basecode.android.data.mapper

import id.basecode.android.data.models.CharacterLocationEntity
import id.basecode.android.domain.models.CharacterLocation
import javax.inject.Inject

class CharacterLocationMapper @Inject constructor() : Mapper<CharacterLocationEntity, CharacterLocation> {

    override fun mapFromEntity(type: CharacterLocationEntity): CharacterLocation {
        return CharacterLocation(name = type.name, url = type.url)
    }

    override fun mapToEntity(type: CharacterLocation): CharacterLocationEntity {
        return CharacterLocationEntity(name = type.name, url = type.url)
    }
}
