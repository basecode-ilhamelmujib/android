package id.basecode.android.data.fakes

import id.basecode.android.data.models.CharacterEntity
import id.basecode.android.data.models.CharacterLocationEntity
import id.basecode.android.domain.models.CharacterLocation

object FakeCharacters {
    fun getCharacters(): List<CharacterEntity> = listOf(
        CharacterEntity(
            "01/02/2021",
            "Male",
            1,
            "https://dummyurl.png",
            CharacterLocationEntity("Earth", "https://dummy.url"),
            "Rick",
            "Human",
            "Alive",
            "",
            "",
            false
        ),
        CharacterEntity(
            "01/02/2021",
            "Male",
            2,
            "https://dummyurl.png",
            CharacterLocationEntity("Earth", "https://dummy.url"),
            "Morty",
            "Human",
            "Alive",
            "",
            "",
            false
        )
    )

    fun getCharacter(): Character =
        Character(
            "01/02/2021",
            "Male",
            1,
            "https://dummyurl.png",
            CharacterLocation("Earth", "https://dummy.url"),
            "Rick",
            "Human",
            "Alive",
            "",
            "",
            false
        )
}
