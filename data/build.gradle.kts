import dependencies.DataDependencies

plugins {
    id(Config.Plugins.kotlin)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    // Modules
    implementation(project(Modules.domain))
    // Kotlin
    implementation(DataDependencies.kotlin)
    // Coroutines
    implementation(DataDependencies.coroutineCore)
    // JavaX
    implementation(DataDependencies.javax)
    // Test Dependencies
    testImplementation(DataDependencies.Test.junit)
    testImplementation(DataDependencies.Test.assertJ)
    testImplementation(DataDependencies.Test.mockitoKotlin)
    testImplementation(DataDependencies.Test.mockitoInline)
    testImplementation(DataDependencies.Test.coroutines)
}
