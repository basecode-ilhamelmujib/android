import dependencies.LocalDependencies

plugins {
    id(Config.Plugins.androidLibrary)
    id(Config.Plugins.kotlinAndroid)
    id(Config.Plugins.kotlinKapt)
}

android {
    compileSdk = Config.Android.androidCompileSdkVersion

    defaultConfig {
        minSdk = Config.Android.androidMinSdkVersion
        targetSdk = Config.Android.androidTargetSdkVersion
        testInstrumentationRunner = Config.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    // Modules
    implementation(project(Modules.data))
    // Kotlin
    implementation(LocalDependencies.kotlin)
    // JavaX
    implementation(LocalDependencies.javax)
    // Room
    LocalDependencies.room.forEach {
        api(it)
    }
    kapt(LocalDependencies.roomKapt)
    // Test Dependencies
    testImplementation(LocalDependencies.Test.junit)
    testImplementation(LocalDependencies.Test.assertJ)
    testImplementation(LocalDependencies.Test.coroutines)
    testImplementation(LocalDependencies.Test.testCore)
    testImplementation(LocalDependencies.Test.testExtJunit)
    testImplementation(LocalDependencies.Test.robolectric)
    testImplementation(LocalDependencies.Test.roomTest)
}
