import dependencies.RemoteDependencies

plugins {
    id(Config.Plugins.kotlin)
    id(Config.Plugins.javaLibrary)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    // Modules
    implementation(project(Modules.data))
    // Kotlin
    implementation(RemoteDependencies.kotlin)
    // JavaX
    implementation(RemoteDependencies.javax)
    // Network (Retrofit, OkHttp, Interceptor, Moshi)
    RemoteDependencies.retrofit.forEach { implementation(it) }
    // Coroutines
    implementation(RemoteDependencies.coroutineCore)

    // Test Dependencies
    testImplementation(RemoteDependencies.Test.junit)
    testImplementation(RemoteDependencies.Test.assertJ)
    testImplementation(RemoteDependencies.Test.mockitoKotlin)
    testImplementation(RemoteDependencies.Test.mockitoInline)
    testImplementation(RemoteDependencies.Test.coroutines)
}
