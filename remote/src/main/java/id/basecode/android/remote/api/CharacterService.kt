package id.basecode.android.remote.api

import id.basecode.android.remote.models.CharacterModel
import id.basecode.android.remote.models.CharacterResponseModel
import retrofit2.http.GET
import retrofit2.http.Path

interface CharacterService {

    @GET("character")
    suspend fun getCharacters(): CharacterResponseModel

    @GET("character/{id}")
    suspend fun getCharacter(@Path("id") id: Long): CharacterModel
}
