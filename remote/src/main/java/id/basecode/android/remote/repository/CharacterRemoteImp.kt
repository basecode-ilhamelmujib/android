package id.basecode.android.remote.repository

import id.basecode.android.data.models.CharacterEntity
import id.basecode.android.data.repository.CharacterRemote
import id.basecode.android.remote.api.CharacterService
import id.basecode.android.remote.mappers.CharacterEntityMapper
import javax.inject.Inject

class CharacterRemoteImp @Inject constructor(
    private val characterService: CharacterService,
    private val characterEntityMapper: CharacterEntityMapper
) : CharacterRemote {

    override suspend fun getCharacters(): List<CharacterEntity> {
        return characterService.getCharacters().characters.map { characterModel ->
            characterEntityMapper.mapFromModel(characterModel)
        }
    }

    override suspend fun getCharacter(characterId: Long): CharacterEntity {
        return characterEntityMapper.mapFromModel(characterService.getCharacter(characterId))
    }
}
