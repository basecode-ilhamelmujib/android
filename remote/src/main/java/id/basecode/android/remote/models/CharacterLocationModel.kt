package id.basecode.android.remote.models

data class CharacterLocationModel(
    val name: String,
    val url: String
)
