package id.basecode.android.remote.mappers

interface EntityMapper<M, E> {

    fun mapFromModel(model: M): E
}
