import dependencies.AppDependencies

plugins {
    id(Config.Plugins.android)
    id(Config.Plugins.kotlinAndroid)
    id(Config.Plugins.navigationSafArgs)
    id(Config.Plugins.kotlinKapt)
    id(Config.Plugins.dagger)
}

android {
    compileSdk = Config.Android.androidCompileSdkVersion

    defaultConfig {
        applicationId = Environments.Release.appId
        minSdk = Config.Android.androidMinSdkVersion
        targetSdk = Config.Android.androidTargetSdkVersion
        versionCode = Environments.Release.appVersionCode
        versionName = Environments.Release.appVersionName
        testInstrumentationRunner = Config.testRunner

        // Configs
        buildConfigField("String", "BASE_URL", "\"" + Environments.Release.baseUrl + "\"")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Modules
    implementation(project(Modules.domain))
    implementation(project(Modules.data))
    implementation(project(Modules.remote))
    implementation(project(Modules.local))
    implementation(project(Modules.presentation))

    // Core Dependencies
    implementation(AppDependencies.kotlin)
    implementation(AppDependencies.coreKtx)
    implementation(AppDependencies.appCompat)
    implementation(AppDependencies.material)
    implementation(AppDependencies.constraint)
    implementation(AppDependencies.navigationFragmentKtx)
    implementation(AppDependencies.navigationUiKtx)
    implementation(AppDependencies.activityKtx)
    // LifeCycle
    AppDependencies.LifeCycle.forEach {
        implementation(it)
    }
    // Dagger-Hilt
    AppDependencies.DaggerHilt.forEach {
        implementation(it)
    }
    AppDependencies.DaggerHiltKapt.forEach {
        kapt(it)
    }
    // Coroutines
    AppDependencies.Coroutines.forEach {
        implementation(it)
    }
    // Glide
    implementation(AppDependencies.glide)
    kapt(AppDependencies.glideKapt)
    // Timber
    implementation(AppDependencies.timber)
    // Lottie animation
    implementation(AppDependencies.lottie)

    // Test Dependencies
    testImplementation(AppDependencies.Test.junit)
    testImplementation(AppDependencies.Test.assertJ)
    testImplementation(AppDependencies.Test.mockitoKotlin)
    testImplementation(AppDependencies.Test.mockitoInline)
    testImplementation(AppDependencies.Test.coroutines)
    testImplementation(AppDependencies.Test.androidxArchCore)
    testImplementation(AppDependencies.Test.robolectric)
    testImplementation(AppDependencies.Test.testExtJunit)
}
