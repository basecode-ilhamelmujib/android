package id.basecode.android.di

import id.basecode.android.BuildConfig
import id.basecode.android.data.repository.CharacterRemote
import id.basecode.android.remote.api.CharacterService
import id.basecode.android.remote.api.ServiceFactory
import id.basecode.android.remote.repository.CharacterRemoteImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    @Provides
    @Singleton
    fun provideBlogService(): CharacterService {
        return ServiceFactory.create(id.basecode.android.BuildConfig.DEBUG, id.basecode.android.BuildConfig.BASE_URL)
    }

    @Provides
    @Singleton
    fun provideCharacterRemote(characterRemote: CharacterRemoteImp): CharacterRemote {
        return characterRemote
    }
}
