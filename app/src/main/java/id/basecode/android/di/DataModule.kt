package id.basecode.android.di

import id.basecode.android.BuildConfig
import id.basecode.android.data.CharacterRepositoryImp
import id.basecode.android.data.SettingsRepositoryImp
import id.basecode.android.domain.repository.CharacterRepository
import id.basecode.android.domain.repository.SettingsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideCharacterRepository(characterRepository: CharacterRepositoryImp): CharacterRepository =
        characterRepository

    @Provides
    @Singleton
    fun provideSettingRepository(): SettingsRepository =
        SettingsRepositoryImp(id.basecode.android.BuildConfig.VERSION_NAME)
}
