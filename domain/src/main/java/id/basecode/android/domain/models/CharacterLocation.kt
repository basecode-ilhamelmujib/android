package id.basecode.android.domain.models

data class CharacterLocation(
    val name: String,
    val url: String
)
