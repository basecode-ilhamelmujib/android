package id.basecode.android.domain.interactor

import id.basecode.android.domain.repository.CharacterRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import id.basecode.android.domain.models.Character

typealias GetCharacterByIdBaseUseCase = BaseUseCase<Long, Flow<Character>>

class GetCharacterByIdUseCase @Inject constructor(
    private val characterRepository: CharacterRepository
) : GetCharacterByIdBaseUseCase {

    override suspend operator fun invoke(params: Long) = characterRepository.getCharacter(params)
}
