package id.basecode.android.domain.repository

import kotlinx.coroutines.flow.Flow
import id.basecode.android.domain.models.Character

interface CharacterRepository {
    // Remote and cache
    suspend fun getCharacters(): Flow<List<Character>>
    suspend fun getCharacter(characterId: Long): Flow<Character>

    // Cache
    suspend fun saveCharacters(listCharacters: List<Character>)
    suspend fun getBookMarkedCharacters(): Flow<List<Character>>
    suspend fun setCharacterBookmarked(characterId: Long): Flow<Int>
    suspend fun setCharacterUnBookMarked(characterId: Long): Flow<Int>
}
