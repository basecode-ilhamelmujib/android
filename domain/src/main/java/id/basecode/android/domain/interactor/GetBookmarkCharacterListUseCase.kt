package id.basecode.android.domain.interactor

import id.basecode.android.domain.repository.CharacterRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import id.basecode.android.domain.models.Character

typealias GetBookmarkCharacterListBaseUseCase = BaseUseCase<Unit, Flow<List<Character>>>

class GetBookmarkCharacterListUseCase @Inject constructor(
    private val characterRepository: CharacterRepository
) : GetBookmarkCharacterListBaseUseCase {

    override suspend operator fun invoke(params: Unit) = characterRepository.getBookMarkedCharacters()
}
