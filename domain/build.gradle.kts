import dependencies.DomainDependencies

plugins {
    id(Config.Plugins.kotlin)
    id(Config.Plugins.javaLibrary)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation(DomainDependencies.kotlin)
    implementation(DomainDependencies.coroutineCore)

    // JavaX
    implementation(DomainDependencies.javax)

    // Test Dependencies
    testImplementation(DomainDependencies.Test.junit)
    testImplementation(DomainDependencies.Test.assertJ)
    testImplementation(DomainDependencies.Test.mockitoKotlin)
    testImplementation(DomainDependencies.Test.mockitoInline)
    testImplementation(DomainDependencies.Test.coroutines)
}
