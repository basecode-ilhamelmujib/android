package dependencies

object DomainDependencies {
    const val kotlin = GlobalDependencies.Kotlin.kotlin
    const val javax = GlobalDependencies.Java.javax
    const val coroutineCore = GlobalDependencies.Coroutines.coroutineCore

    object Test {
        const val junit = GlobalDependencies.TestDep.junit
        const val coroutines = GlobalDependencies.TestDep.coroutinesTest
        const val mockitoKotlin = GlobalDependencies.TestDep.mockitoKotlin
        const val mockitoInline = GlobalDependencies.TestDep.mockitoInline
        const val assertJ = GlobalDependencies.TestDep.assertJ
    }
}