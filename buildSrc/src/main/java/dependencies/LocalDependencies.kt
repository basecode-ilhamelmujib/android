package dependencies

object LocalDependencies {
    const val kotlin = GlobalDependencies.Kotlin.kotlin
    const val javax = GlobalDependencies.Java.javax
    val room = listOf(GlobalDependencies.Room.roomRuntime, GlobalDependencies.Room.roomKtx)
    const val roomKapt = GlobalDependencies.Room.roomCompilerKapt

    object Test {
        const val junit = GlobalDependencies.TestDep.junit
        const val coroutines = GlobalDependencies.TestDep.coroutinesTest
        const val assertJ = GlobalDependencies.TestDep.assertJ
        const val testCore = GlobalDependencies.TestDep.testCore
        const val testExtJunit = GlobalDependencies.TestDep.testExtJunit
        const val robolectric = GlobalDependencies.TestDep.robolectric
        const val roomTest = GlobalDependencies.TestDep.roomTest
    }
}