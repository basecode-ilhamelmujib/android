package dependencies

object PresentationDependencies {
    const val kotlin = GlobalDependencies.Kotlin.kotlin
    const val javax = GlobalDependencies.Java.javax
    const val coroutineCore = GlobalDependencies.Coroutines.coroutineCore

    // Dagger-Hilt
    val daggerHilt = listOf(
        GlobalDependencies.DaggerHilt.hiltAndroid
    )
    val daggerHiltKapt = listOf(
        GlobalDependencies.DaggerHilt.hiltAndroidKapt,
        GlobalDependencies.DaggerHilt.hiltKapt
    )
    val lifeCycle = listOf(
        GlobalDependencies.LifeCycle.viewModelKtx,
        GlobalDependencies.LifeCycle.liveDataKtx,
        GlobalDependencies.LifeCycle.lifeCycleExtension,
        GlobalDependencies.LifeCycle.lifeCycleRuntime,
        GlobalDependencies.LifeCycle.lifeCycleRuntimeKtx
    )

    object Test {
        const val junit = GlobalDependencies.TestDep.junit
        const val coroutines = GlobalDependencies.TestDep.coroutinesTest
        const val mockitoKotlin = GlobalDependencies.TestDep.mockitoKotlin
        const val mockitoInline = GlobalDependencies.TestDep.mockitoInline
        const val assertJ = GlobalDependencies.TestDep.assertJ
        const val androidxArchCore = GlobalDependencies.TestDep.androidxArchCore
        const val robolectric = GlobalDependencies.TestDep.robolectric
        const val testExtJunit = GlobalDependencies.TestDep.testExtJunit
    }

}