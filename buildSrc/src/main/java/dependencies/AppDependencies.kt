package dependencies

object AppDependencies {

    // Kotlin
    const val kotlin = GlobalDependencies.Kotlin.kotlin

    // Core
    const val coreKtx = GlobalDependencies.Core.coreKtx
    const val appCompat = GlobalDependencies.Core.appCompat
    const val material = GlobalDependencies.Core.material
    const val constraint = GlobalDependencies.Core.constraint
    const val navigationFragmentKtx = GlobalDependencies.Core.navigationFragmentKtx
    const val navigationUiKtx = GlobalDependencies.Core.navigationUiKtx
    const val activityKtx = GlobalDependencies.Core.activityKtx

    // LifeCycle
    val LifeCycle = listOf(
        GlobalDependencies.LifeCycle.viewModelKtx,
        GlobalDependencies.LifeCycle.lifeCycleExtension,
        GlobalDependencies.LifeCycle.lifeCycleRuntime,
        GlobalDependencies.LifeCycle.lifeCycleRuntimeKtx
    )

    // Hilt
    val DaggerHilt = listOf(
        GlobalDependencies.DaggerHilt.hiltAndroid
    )

    val DaggerHiltKapt = listOf(
        GlobalDependencies.DaggerHilt.hiltAndroidKapt,
        GlobalDependencies.DaggerHilt.hiltKapt
    )

    // Coroutines
    val Coroutines = listOf(
        GlobalDependencies.Coroutines.coroutineCore,
        GlobalDependencies.Coroutines.coroutineAndroid
    )

    const val glide = GlobalDependencies.Glide.glide
    const val glideKapt = GlobalDependencies.Glide.glideKapt
    const val timber = GlobalDependencies.Timber.timber
    const val lottie = GlobalDependencies.Lottie.lottie

    object Test {
        const val junit = GlobalDependencies.TestDep.junit
        const val coroutines = GlobalDependencies.TestDep.coroutinesTest
        const val mockitoKotlin = GlobalDependencies.TestDep.mockitoKotlin
        const val mockitoInline = GlobalDependencies.TestDep.mockitoInline
        const val assertJ = GlobalDependencies.TestDep.assertJ
        const val androidxArchCore = GlobalDependencies.TestDep.androidxArchCore
        const val robolectric = GlobalDependencies.TestDep.robolectric
        const val testExtJunit = GlobalDependencies.TestDep.testExtJunit
    }
}