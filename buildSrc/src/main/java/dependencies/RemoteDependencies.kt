package dependencies

object RemoteDependencies {
    const val kotlin = GlobalDependencies.Kotlin.kotlin
    const val javax = GlobalDependencies.Java.javax
    val retrofit = listOf(
        GlobalDependencies.Retrofit.retrofit,
        GlobalDependencies.Retrofit.moshiConverter,
        GlobalDependencies.Retrofit.loggingInterceptor
    )
    const val coroutineCore = GlobalDependencies.Coroutines.coroutineCore

    object Test {
        const val junit = GlobalDependencies.TestDep.junit
        const val coroutines = GlobalDependencies.TestDep.coroutinesTest
        const val mockitoKotlin = GlobalDependencies.TestDep.mockitoKotlin
        const val mockitoInline = GlobalDependencies.TestDep.mockitoInline
        const val assertJ = GlobalDependencies.TestDep.assertJ
    }
}