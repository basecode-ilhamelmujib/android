object Versions {

    const val gradleVersion = "7.4.1"
    const val gradleNavigationArgVersion = "2.5.3"
    const val kotlinVersion = "1.9.0"
    const val ktLintVersion = "10.1.0"

    // Core dependencies
    const val javaxInjectVersion = "1"
    const val coreKtxVersion = "1.7.0"
    const val appCompatVersion = "1.3.1"
    const val materialVersion = "1.4.0"
    const val constraintLayoutVersion = "2.1.0"

    // Activity KTX for viewModels() dependencies
    const val activityKtxVersion = "1.7.0"

    // Navigation component dependencies
    const val navFragmentKtxVersion = "2.5.3"
    const val navUiKtxVersion = "2.5.3"

    // LifeCycle dependencies
    const val viewmodelKtxVersion = "2.4.0"
    const val lifeCycleExtVersion = "2.2.0"
    const val liveDataKtxVersion = "2.4.0"
    const val lifeCycleRuntimeVersion = "2.4.0"
    const val lifeCycleRuntimeKtxVersion = "2.4.0"

    // Coroutines dependencies
    const val coroutineCoreVersion = "1.5.2"
    const val coroutineAndroidVersion = "1.5.2"

    // Glide dependencies
    const val glideVersion = "4.12.0"

    // Dagger - Hilt dependencies
    const val hiltAndroidVersion = "2.48"
    const val hiltVersion = "1.1.0"

    // Timber logging dependencies
    const val timberVersion = "5.0.1"

    // Network dependencies
    const val retrofitVersion = "2.9.0"
    const val moshiConverterVersion = "2.9.0"
    const val loggingInterceptorVersion = "4.12.0"

    // Room
    const val roomVersion = "2.4.0"

    // Lottie animation
    const val lottieVersion = "3.7.0"

    // Testing
    const val junitVersion = "4.13"
    const val androidxArchCoreVersion = "2.1.0"
    const val coroutinesTestVersion = "1.5.2"
    const val mockitoCoreVersion = "3.12.4"
    const val mockitoInlineVersion = "3.12.4"
    const val mockitoAndroidVersion = "3.12.4"
    const val mockitoKotlinVersion = "2.2.0"
    const val assertJVersion = "3.19.0"
    const val extJunitVersion = "1.1.1"
    const val androidTestRuleVersion = "1.1.0"
    const val androidTestRunnerVersion = "1.1.0"
    const val axTestCore = "1.1.0"
    const val axTestJunit = "1.1.0"
    const val espressoVersion = "3.2.0"
    const val robolectricVersion = "4.5.1"

}
