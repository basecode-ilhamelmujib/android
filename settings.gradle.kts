rootProject.name = "Base Code Android"
include(":domain")
include(":data")
include(":remote")
include(":local")
include(":presentation")
include(":app")
