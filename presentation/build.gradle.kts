import dependencies.PresentationDependencies

plugins {
    id(Config.Plugins.androidLibrary)
    id(Config.Plugins.kotlinAndroid)
    id(Config.Plugins.kotlinKapt)
}

android {
    compileSdk = Config.Android.androidCompileSdkVersion
    defaultConfig {
        minSdk = Config.Android.androidMinSdkVersion
        targetSdk = Config.Android.androidTargetSdkVersion
        testInstrumentationRunner = Config.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(project(Modules.domain))

    implementation(PresentationDependencies.kotlin)
    implementation(PresentationDependencies.coroutineCore)
    // Dagger-Hilt (used for @InjectViewModel)
    PresentationDependencies.daggerHilt.forEach {
        implementation(it)
    }
    PresentationDependencies.daggerHiltKapt.forEach {
        kapt(it)
    }
    // JavaX
    implementation(PresentationDependencies.javax)
    // LifeCycle
    PresentationDependencies.lifeCycle.forEach {
        implementation(it)
    }

    // Test Dependencies
    testImplementation(PresentationDependencies.Test.junit)
    testImplementation(PresentationDependencies.Test.assertJ)
    testImplementation(PresentationDependencies.Test.mockitoKotlin)
    testImplementation(PresentationDependencies.Test.mockitoInline)
    testImplementation(PresentationDependencies.Test.coroutines)
    testImplementation(PresentationDependencies.Test.androidxArchCore)
    testImplementation(PresentationDependencies.Test.robolectric)
    testImplementation(PresentationDependencies.Test.testExtJunit)
}
